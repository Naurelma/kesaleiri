/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vko9;

/**
 *
 * @author p4464
 */
public class ElokuvaTeatteri {

    private final String paikka;
    private final int id;

    public ElokuvaTeatteri(int i, String nimi) {
	this.id = i;
	paikka = nimi;
    }

    @Override
    public String toString() {
	return paikka;
    }

    public int getId() {
	return id;
    }

}
