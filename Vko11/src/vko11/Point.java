/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vko11;

import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Circle;

/**
 *
 * @author p4464
 */
public class Point {

    private final Circle c;

    public Point(double x, double y) {
	c = new Circle(10);
	c.setLayoutX(x);
	c.setLayoutY(y);
	EventHandler<MouseEvent> eventHandler = new EventHandler<MouseEvent>() {
	    @Override
	    public void handle(MouseEvent e) {
		System.out.println("Hei, olen piste!");
		ShapeHandler.getInstance().viivaPiste(c.getLayoutX(), c.getLayoutY());

		e.consume();

		((Node) e.getSource()).fireEvent(new MyEvent(MyEvent.Piirrä));

	    }
	};
	//Registering the event filter 
	c.addEventFilter(MouseEvent.MOUSE_CLICKED, eventHandler);
    }

    public Circle getC() {
	return c;
    }

}
