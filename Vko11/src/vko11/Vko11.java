/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vko11;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author p4464
 */
public class Vko11 extends Application {

    @Override
    public void start(Stage stage) throws Exception {
	Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));

	//String osoite = "https://photos-1.dropbox.com/t/2/AADfAisk6DXJS_78WVMlOhws3XQhswqn195TB1HbheTeOg/12/69560808/jpeg/32x32/3/1528210800/0/2/Suomen-kartta.jpg/EPmGjDYYw6gDIAcoBw/8fmWY51oXplFNcjOjfnUyiNBzoWjQVBJsro3dR2IVUI?dl=0&size=1024x768&size_mode=3";
	String osoite = getClass().getResource("kartta.jpeg").toExternalForm();
	root.setStyle("-fx-background-image:url('" + osoite + "'  ); -fx-background-repeat: no-repeat;-fx-background-position: center;");

	Scene scene = new Scene(root);
	stage.setResizable(false);
	stage.setScene(scene);
	stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

	launch(args);

    }

    //Event Handling vaatii töitä
}
