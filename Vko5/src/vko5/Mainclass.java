/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vko5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author p4464
 */
public class Mainclass {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
	Character r = new Character(4);
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	Character hahmo = r.luo(1, 2);

	int valinta;
	int num;
	int ase;
	do {
	    System.out.println("*** TAISTELUSIMULAATTORI ***");
	    System.out.println("1) Luo hahmo");
	    System.out.println("2) Taistele hahmolla");
	    System.out.println("0) Lopeta");
	    System.out.print("Valintasi: ");
	    valinta = Integer.parseInt(br.readLine());

	    if (valinta == 1) {
		System.out.println("Valitse hahmosi: ");
		System.out.println("1) Kuningas");
		System.out.println("2) Ritari");
		System.out.println("3) Kuningatar");
		System.out.println("4) Peikko");
		System.out.print("Valintasi: ");
		num = Integer.parseInt(br.readLine());
		//Numeron tarkistus on jätetty pois.
		System.out.println("Valitse aseesi: ");
		System.out.println("1) Veitsi");
		System.out.println("2) Kirves");
		System.out.println("3) Miekka");
		System.out.println("4) Nuija");
		System.out.print("Valintasi: ");
		ase = Integer.parseInt(br.readLine());
		hahmo = r.luo(num, ase);
	    } else if (valinta == 2) {

		System.out.println(hahmo.fight());
	    }

	} while (valinta != 0);

    }
}
