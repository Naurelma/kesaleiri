package vko6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author p4464
 */
public class Mainclass {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
	Bank op = new Bank();
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	int valinta, raha, raja;

	String numero;
	do {
	    System.out.println("\n*** PANKKIJÄRJESTELMÄ ***");
	    System.out.println("1) Lisää tavallinen tili");
	    System.out.println("2) Lisää luotollinen tili");
	    System.out.println("3) Tallenna tilille rahaa");
	    System.out.println("4) Nosta tililtä");
	    System.out.println("5) Poista tili");
	    System.out.println("6) Tulosta tili");
	    System.out.println("7) Tulosta kaikki tilit");
	    System.out.println("0) Lopeta");
	    System.out.print("Valintasi: ");

	    valinta = Integer.parseInt(br.readLine());
	    switch (valinta) {
		case 0:
		    break;
		case 1:
		    System.out.print("Syötä tilinumero: ");
		    numero = br.readLine();
		    System.out.print("Syötä rahamäärä: ");
		    raha = Integer.parseInt(br.readLine());

		    op.luoTili(numero, raha);

		    break;
		case 2:
		    System.out.print("Syötä tilinumero: ");
		    numero = br.readLine();
		    System.out.print("Syötä rahamäärä: ");
		    raha = Integer.parseInt(br.readLine());
		    System.out.print("Syötä luottoraja: ");
		    raja = Integer.parseInt(br.readLine());

		    op.luoTili(numero, raha, raja);
		    break;
		case 3:
		    System.out.print("Syötä tilinumero: ");
		    numero = br.readLine();
		    System.out.print("Syötä rahamäärä: ");
		    raha = Integer.parseInt(br.readLine());

		    op.talleta(numero, raha);
		    break;
		case 4:
		    System.out.print("Syötä tilinumero: ");
		    numero = br.readLine();
		    System.out.print("Syötä rahamäärä: ");
		    raha = Integer.parseInt(br.readLine());

		    op.nosta(numero, raha);
		    break;
		case 5:
		    System.out.print("Syötä poistettava tilinumero: ");
		    numero = br.readLine();
		    op.poistaTili(numero);
		    break;
		case 6:
		    System.out.print("Syötä tulostettava tilinumero: ");
		    numero = br.readLine();
		    op.tulosta(numero);
		    break;
		case 7:
		    System.out.println("Kaikki tilit:");
		    op.kaikki();
		    break;
		default:
		    System.out.println("Valinta ei kelpaa.");
		    break;

	    }
	} while (valinta != 0);
    }
}
