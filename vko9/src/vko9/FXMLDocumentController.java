/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vko9;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;

/**
 *
 * @author p4464
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private ComboBox<ElokuvaTeatteri> teatteri;

    @FXML
    private TextField alkuTunnit;
    @FXML
    private TextField alkuMinuutit;
    @FXML
    private TextField loppuTunnit;
    @FXML
    private TextField loppuMinuutit;
    @FXML
    private TextField elokuvanNimi;

    @FXML
    private TextField paiva;
    @FXML
    private TextField kuukausi;
    @FXML
    private TextField vuosi;

    @FXML
    private ListView<String> elokuvat;

    @FXML
    private CheckBox hakuKaikista;

    private Integer day;
    private Integer month;
    private Integer year;

    @FXML
    private void handleListaaminen(ActionEvent event) {
	System.out.println("Listaa");
	elokuvanNimi.setText("");
	ElokuvaTeatteri value = teatteri.getValue();

	if (value != null) {
	    String s = pmv();
	    NäytösLista nl = NäytösLista.getInstance(value.getId(), s);

	    nl.paivita(haeInt(alkuTunnit), haeInt(alkuMinuutit),
		    haeInt(loppuTunnit), haeInt(loppuMinuutit), "");

	    elokuvat.getItems().clear();
	    elokuvat.getItems().addAll(nl.getNimet());
	}
    }

    private String pmv() {
	String s = "";
	if (!paiva.getText().isEmpty()) {
	    s = s.concat(paiva.getText());
	} else {
	    s += "0" + day.toString();
	}

	if (!kuukausi.getText().isEmpty()) {
	    s = s.concat(kuukausi.getText());
	} else {
	    s += "0" + month.toString();
	}

	if (!vuosi.getText().isEmpty()) {
	    s = s.concat(vuosi.getText());
	} else {
	    s += "0" + year.toString();
	}

	return s;
    }

    private int haeInt(TextField f) {
	try {
	    return Integer.parseInt(f.getText());
	} catch (Exception e) {
	    return -1;
	}
    }

    /*
    @FXML
    private void handleNextDate(KeyEvent e) {
    hoidaDateSiirtymät((TextField) e.getSource());
    }
    
    private void hoidaDateSiirtymät(TextField muokattava) {
    try {
    int arvo = Integer.parseInt(muokattava.getText());
    if (muokattava.getText().trim().length() > 2) {
    if (muokattava.equals(päivä)) {
    if (arvo > 32) {
    muokattava.setText("32");
    }
    kuukausi.requestFocus();
    
    } else if (muokattava.equals(kuukausi)) {
    if (arvo > 12) {
    muokattava.setText("12");
    }
    vuosi.requestFocus();
    } else if (muokattava.equals(vuosi)) {
    päivä.requestFocus();
    }
    }
    } catch (Exception ex) {
    muokattava.setText("");
    }
    }
     */
    @FXML
    private void tekstiAlueet(ActionEvent e) {
	TextField a = (TextField) e.getSource();
	if (!a.getText().isEmpty()) {
	    a.setText("0" + a.getText());
	    vuosi.positionCaret(10);
	    hoidaAikaSiirtymät(a);
	}

    }

    @FXML
    private void handleVuosi(ActionEvent e) {
	TextField a = (TextField) e.getSource();
	if (a.getText().trim().length() == 2) {
	    a.setText("20" + a.getText());
	    vuosi.positionCaret(10);
	}
    }

    @FXML
    private void handleNext(KeyEvent e) {

	hoidaAikaSiirtymät((TextField) e.getSource());

    }

    private void hoidaAikaSiirtymät(TextField muokattava) {
	System.out.println(muokattava);
	try {
	    int arvo = Integer.parseInt(muokattava.getText());
	    if (muokattava.getText().trim().length() == 2) {
		if (arvo < 0) {
		    muokattava.setText("0");
		}
		if (muokattava.equals(alkuTunnit)) {
		    if (arvo > 23) {
			muokattava.setText("23");
		    }
		    alkuMinuutit.requestFocus();
		} else if (muokattava.equals(alkuMinuutit)) {
		    if (arvo > 59) {
			muokattava.setText("59");
		    }
		    loppuTunnit.requestFocus();
		} else if (muokattava.equals(loppuTunnit)) {
		    if (arvo > 23) {
			muokattava.setText("23");
		    }
		    loppuMinuutit.requestFocus();
		} else if (muokattava.equals(loppuMinuutit)) {
		    if (arvo > 59) {
			muokattava.setText("59");
		    }
		    alkuTunnit.requestFocus();

		} else if (muokattava.equals(paiva)) {
		    if (arvo > 31) {
			muokattava.setText("31");
		    }
		    kuukausi.requestFocus();
		} else if (muokattava.equals(kuukausi)) {
		    if (arvo > 12) {
			muokattava.setText("12");
		    }
		    vuosi.requestFocus();
		}
	    } else if (muokattava.getText().trim().length() > 2) {

		muokattava.setText("");
	    }
	} catch (Exception ex) {
	    muokattava.setText("");
	}

    }

    @FXML
    private void handleNimiHaku(ActionEvent event) {

	System.out.println("Nimihaku");
	ElokuvaTeatteri value = null;
	ElokuvaTeatteri original = teatteri.getValue();
	//viimeisen takia erikois järjestely
	teatteri.getSelectionModel().selectLast();
	ElokuvaTeatteri last = teatteri.getValue();
	teatteri.getSelectionModel().select(original);

	NäytösLista nl;
	elokuvat.getItems().clear();
	value = teatteri.getValue();
	int len = TeatteriLista.getInstance().getTeatterit().length;
	System.out.printf("Haettavia kohteita: %d\n", len);
	do {

	    System.out.printf("%d ", value.getId());
	    nl = NäytösLista.getInstance(value.getId(), pmv());
	    nl.paivita(haeInt(alkuTunnit), haeInt(alkuMinuutit),
		    haeInt(loppuTunnit), haeInt(loppuMinuutit),
		    elokuvanNimi.getText());

	    elokuvat.getItems().add("==" + value.toString() + "==");
	    elokuvat.getItems().addAll(nl.getNimet());

	    if (value.equals(last)){
		teatteri.getSelectionModel().selectFirst();
	    } else {
		teatteri.getSelectionModel().selectNext();
	    }
	    value = teatteri.getValue();

	} while (hakuKaikista.isSelected() && !original.equals(value));
	teatteri.getSelectionModel().select(original);
	System.out.println("\nNimilistaus valmis!");
	//Loopataan kaikkien teatterien yli ja valitaan alkuperäinen lopuksi
    }

    private void initPaikat() {

	ObservableList items = teatteri.getItems();
	items.clear();
	TeatteriLista tl = TeatteriLista.getInstance();
	items.addAll(Arrays.asList(tl.getTeatterit()));

	teatteri.getSelectionModel().selectFirst();

    }
//

    @Override
    public void initialize(URL url, ResourceBundle rb) {

	initPaikat();

	String timeStamp = new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
	System.out.println(timeStamp);
	String[] split = timeStamp.split("/");
	System.out.println(split[0]);

	day = Integer.parseInt(split[0]);
	month = Integer.parseInt(split[1]);
	year = Integer.parseInt(split[2]);

	paiva.setPromptText(split[0]);
	kuukausi.setPromptText(split[1]);
	vuosi.setPromptText(split[2]);

    }

}
