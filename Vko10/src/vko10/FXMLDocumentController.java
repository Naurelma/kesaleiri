/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vko10;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.concurrent.Worker.State;
import javafx.scene.web.WebView;

/**
 *
 * @author p4464
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private TextField osoiteBar;
    @FXML
    private WebView wv;
    @FXML
    private Button lataa;

    private String last = "";
    private String next = "";
    private String current = "";

    @Override
    public void initialize(URL url, ResourceBundle rb) {
	wv.getEngine().load("http://google.com");
	paivitaOsoite();

	wv.getEngine().getLoadWorker().stateProperty().addListener(
		new ChangeListener<State>() {
	    @Override
	    public void changed(ObservableValue ov, State oldState, State newState) {

		if (newState == Worker.State.SUCCEEDED) {
		    last = current;
		    current = wv.getEngine().getLocation();
		    paivitaOsoite();
		    System.out.println("called");
		}

	    }
	});

    }

    private void paivitaOsoite() {

	try {
	    osoiteBar.setText(wv.getEngine().getLocation().split("://", 2)[1]);
	} catch (Exception ex) {
	    osoiteBar.setText(wv.getEngine().getLocation());
	}
    }

    @FXML
    private void handleReload(ActionEvent event
    ) {
	wv.getEngine().reload();
	paivitaOsoite();
    }

    @FXML
    private void handleButtonAction(ActionEvent event) {
	avaaSivu(osoiteBar.getText());

    }

    private void avaaSivu(String text) {
	if (text == null) {
	    return;
	} else if (text.isEmpty()) {
	    return;
	}
	if (text.equals("index.html")) {
	    URL url = this.getClass().getResource("index.html");
	    text = url.toString();
	} else if (!text.startsWith("http://")) {
	    text = "http://" + text;
	}
	wv.getEngine().load(text);
	paivitaOsoite();
	System.out.println(wv.getEngine().getLocation());

    }

    @FXML
    private void huuda(ActionEvent event) {
	try {
	    wv.getEngine().executeScript("document.shoutOut()");
	} catch (Exception e) {

	}
    }

    @FXML
    private void palauta(ActionEvent event) {
	try {
	    wv.getEngine().executeScript("initialize()");
	} catch (Exception e) {

	}
    }

    @FXML
    private void edellinen(ActionEvent event) {
	next = osoiteBar.getText();
	osoiteBar.setText(last);
	avaaSivu(last);
	paivitaOsoite();
    }

    @FXML
    private void seuraava(ActionEvent event) {
	System.out.printf("%s %s %s \n", last, current, next);
	last = osoiteBar.getText();
	osoiteBar.setText(next);
	avaaSivu(next);
	paivitaOsoite();
    }

}
