/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vko11;

import java.util.ArrayList;
import javafx.scene.Node;
import javafx.scene.shape.Line;

/**
 *
 * @author p4464
 */
public class ShapeHandler {

    private static ShapeHandler instance = null;

    private ArrayList<Point> ympyrät;
    private Line linja = null;
    private Double alkuX = null;
    private Double alkuY = null;

    private ShapeHandler() {
	ympyrät = new ArrayList<>();

    }

    public static ShapeHandler getInstance() {
	if (instance == null) {
	    instance = new ShapeHandler();
	}
	return instance;
    }

    public void addCircle(double x, double y) {
	Point c = new Point(x, y);
	ympyrät.add(c);
    }

    public ArrayList<Node> getKuviot() {
	ArrayList<Node> ar = new ArrayList<>();
	for (Point p : ympyrät) {
	    ar.add(p.getC());
	}
	if (linja != null) {
	    ar.add(linja);
	}
	return ar;
    }

    public void viivaPiste(Double x, Double y) {
	if (alkuX == null && alkuY == null) {
	    alkuX = x;
	    alkuY = y;
	} else {
	    //System.out.printf("%f, %f, %f, %f\n", alkuX, alkuY, x,y);
	    linja = new Line();
	    linja.setStartX(alkuX);
	    linja.setStartY(alkuY);
	    linja.setEndX(x);
	    linja.setEndY(y);

	    alkuX = null;
	    alkuY = null;
	    System.out.println(linja);
	}
    }

}
