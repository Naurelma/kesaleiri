/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vko9;

import java.io.IOException;
import java.io.StringReader;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import org.xml.sax.SAXException;

public class TeatteriLista {

    private static TeatteriLista instanssi;
    private ElokuvaTeatteri[] teatterit;

    private TeatteriLista() {
	String alueet = "https://www.finnkino.fi/xml/TheatreAreas/";

	teatterit = parse(PageLoader.gatherData(alueet));
    }

    public static TeatteriLista getInstance() {
	if (instanssi == null) {
	    instanssi = new TeatteriLista();
	}
	return instanssi;
    }

    public ElokuvaTeatteri[] getTeatterit() {
	return teatterit;
    }

    private ElokuvaTeatteri[] parse(String total) {
	Document doc;
	try {
	    DocumentBuilderFactory dbFactory
		    = DocumentBuilderFactory.newInstance();
	    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

	    doc = dBuilder.parse(new InputSource(new StringReader(total)));
	    doc.getDocumentElement().normalize();

	    NodeList arvot = doc.getElementsByTagName("TheatreArea");
	    ElokuvaTeatteri et[] = new ElokuvaTeatteri[arvot.getLength()];
	    for (int i = 0; i < arvot.getLength(); i++) {
		Element item = (Element) arvot.item(i);
		int id = Integer.parseInt(item.getElementsByTagName("ID").item(0).getTextContent());
		String nimi = item.getElementsByTagName("Name").item(0).getTextContent();
		et[i] = new ElokuvaTeatteri(id, nimi);

	    }
	    return et;

	} catch (ParserConfigurationException e) {
	    System.err.println("Caught ParserConfigurationException!");
	} catch (IOException e) {
	    System.err.println("Caught IOException!");
	} catch (SAXException e) {
	    System.err.println("Caught SAXException!");
	}
	return null;
    }
}
