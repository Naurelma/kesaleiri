/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vko5;

/**
 *
 * @author p4464
 */
public class Car {
    private Kori kori;
    private Alusta alusta;
    private Moottori moottori;
    private Rengas renkaat[];
    
    public Car(){
	kori = new Kori();
	alusta = new Alusta();
	moottori = new Moottori();
	renkaat = new Rengas[4];
	for (int i = 0; i < renkaat.length; i++) {
	    renkaat[i] = new Rengas();
	}
	
    }
    
    public void display(){
	System.out.println("Autoon kuuluu:\n\tBody\n\tChassis\n\tEngine\n\t4 Wheel");
    }
    private class Osa {

	private int paino;
	private int ikä;
	private String valmistaja;

    }

    public class Kori extends Osa {

	private String väri;
	private int PenkkienLkm;

	public Kori() {
	    System.out.println("Valmistetaan: Body");
	}
    }

    public class Alusta extends Osa {

	private int rengasPaikkojenLkm;

	public Alusta() {
	    System.out.println("Valmistetaan: Chassis");
	}
    }

    public class Moottori extends Osa {

	private int kierrosLkm;
	private int maksimiKierrosLkm;
	private boolean käynnissä;
	private double kulutus;

	public Moottori() {
	    System.out.println("Valmistetaan: Engine");
	}
    }

    public class Rengas extends Osa {
	private String pinnanKuviointi;
	private double pinnanKunto;
	
	public Rengas(){
	    System.out.println("Valmistetaan: Wheel");
	}
    }

}

//<script>alert("HEllo")<\script>