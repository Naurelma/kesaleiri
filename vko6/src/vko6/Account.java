package vko6;

/**
 *
 * @author p4464
 */
public abstract class Account {

    protected String tilinumero;
    protected int rahaMäärä;

    public Account(String numero, int raha) {
	tilinumero = numero;
	rahaMäärä = raha;
    }

    public void esitä() {
	System.out.printf("Tilinumero: %s ", tilinumero);
	System.out.printf("Tilillä rahaa: %d", rahaMäärä);
    }

    public void nosta(int määrä) {
	if (rahaMäärä - määrä < 0) {
	    System.out.println("Rahat eivät riitä");
	} else {
	    rahaMäärä -= määrä;
	}
    }

    public void talleta(int määrä) {
	rahaMäärä += määrä;
    }

    public static class Tili extends Account {

	public Tili(String numero, int raha) {
	    super(numero, raha);
	}

    }

    public static class LuottoTili extends Tili {

	private final int luottoRaja;

	public LuottoTili(String numero, int määrä, int raja) {
	    super(numero, määrä);
	    luottoRaja = raja;
	}

	@Override
	public void nosta(int määrä) {
	    if (luottoRaja + rahaMäärä - määrä < 0) {
		System.out.println("Luottoraja ylitetty!");
	    } else {
		rahaMäärä -= määrä;
	    }
	}

	@Override
	public void esitä() {
	    super.esitä();
	    System.out.printf(" Luottoraja: %d", luottoRaja);

	}
    }
}
