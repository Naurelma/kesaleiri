package vko4;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 *
 * @author p4464
 */
public class ReadAndWriteIO {

    public void readFile(String name) {
	try {
	    ZipFile zf = new ZipFile("zipinput.zip");
	    Enumeration<? extends ZipEntry> entries = zf.entries();
	    ZipEntry entry = entries.nextElement();
	    InputStream inputStream = zf.getInputStream(entry);
	    
	    BufferedReader in;
	    
	    in = new BufferedReader(new InputStreamReader(inputStream));

	    String linja;

	    linja = in.readLine();
	    while (linja != null) {
		System.out.println(linja);
		linja = in.readLine();
	    }
	    in.close();
	    zf.close();
	} catch (FileNotFoundException ex) {
	    System.out.println("Tiedostoa ei löydy");
	} catch (IOException ex) {
	    System.out.println("Luku epäonnistui");
	}
    }

    public void readAndWrite(String lahde, String kohde) {
	try {
	    BufferedReader in = new BufferedReader(new FileReader(lahde));
	    BufferedWriter out = new BufferedWriter(new FileWriter(kohde));

	    String linja;

	    linja = in.readLine();
	    while (linja != null) {
		if (linja.length() < 30 && !linja.trim().isEmpty() && linja.contains("v")) {
		    out.write(linja);
		    out.write("\n");
		}
		linja = in.readLine();
	    }
	    in.close();
	    out.flush();
	    out.close();
	} catch (FileNotFoundException ex) {
	    System.out.println("Tiedostoa ei löydy");
	} catch (IOException ex) {
	    System.out.println("Luku epäonnistui");
	}
    }
}
