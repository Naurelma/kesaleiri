/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vko7t5;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 *
 * @author p4464
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private TextArea kentta;

    @FXML
    private TextField tiedostoNimi;

    private String nimi;

    @FXML
    private void handleAvaa(ActionEvent event) {
	BufferedReader br = null;
	try {
	    System.out.println("You clicked Avaa!");
	    br = new BufferedReader(new FileReader(tiedostoNimi.getText()));
	    String s;
	    kentta.setText("");
	    s = br.readLine();
	    while (s != null) {
		kentta.appendText(s);
		s = br.readLine();
	    }

	} catch (FileNotFoundException ex) {
	    System.out.println("Tiedostoa ei löytynyt.");
	} catch (IOException ex) {
	    System.out.println("Lukeminen epäonnistui");
	} finally {
	    try {
		if (br != null) {
		    br.close();

		}
		System.out.println("Tiedosto suljettu.");
	    } catch (IOException ex) {
		System.out.append("Sulkeminen epäonnistui.");
	    }
	}
	System.out.println("Done With Avaa!");

    }

    @FXML
    private void handleTallenna(ActionEvent event) {
	BufferedWriter bw = null;
	try {
	    System.out.println("You clicked Tallenna!");
	    bw = new BufferedWriter(new FileWriter(tiedostoNimi.getText()));
	    bw.write(kentta.getText());
	} catch (FileNotFoundException ex) {
	    System.out.println("Tiedostoa ei löytynyt.");
	} catch (IOException ex) {
	    System.out.println("Virhe!");
	} finally {
	    try {
		if (bw != null) {

		    bw.close();
		}
		System.out.println("Tiedosto suljettu.");
	    } catch (IOException ex) {
		System.out.append("Sulkeminen epäonnistui.");
	    }
	}
	System.out.println("Done With Tallenna!");

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
	// TODO
    }

}
