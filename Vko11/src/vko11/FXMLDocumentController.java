/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vko11;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.SwipeEvent;
import javafx.scene.input.TouchEvent;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author p4464
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private AnchorPane anchor;

    private void handleButtonAction(ActionEvent event) {
	System.out.println("You clicked me!");

    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
	// TODO

	anchor.addEventHandler(MyEvent.Piirrä, new EventHandler<MyEvent>() {
	    @Override
	    public void handle(MyEvent t) {
		paivita();
	    }
	});
    }

    private void paivita() {
	anchor.getChildren().clear();
	anchor.getChildren().addAll(ShapeHandler.getInstance().getKuviot());

    }

    @FXML
    private void handleClick(MouseEvent event) {
	ShapeHandler sh = ShapeHandler.getInstance();
	sh.addCircle(event.getX(), event.getY());

	System.out.println("Uusi");
	((Node) event.getSource()).fireEvent(new MyEvent(MyEvent.Piirrä));

    }

    @FXML
    private void handleButtonAction(SwipeEvent event) {
    }

    @FXML
    private void handleT(TouchEvent event) {

    }

}
