package pullokone;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author p4464
 */
public class Mainclass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
	BottleDispenser bd = new BottleDispenser();
	BufferedReader in;
	String inputLine;
	int valinta;
	in = new BufferedReader(new InputStreamReader(System.in));

	do {
	    valikko();
	    valinta = Integer.parseInt(in.readLine());
	    switch (valinta) {
		case 0:
		    break;
		case 1:
		    bd.addMoney();
		    break;
		case 2:
		    bd.listaaPullot();
		    System.out.print("Valintasi: ");
		    bd.buyBottle(Integer.parseInt(in.readLine()));
		    break;
		case 3:
		    bd.returnMoney();
		    break;
		case 4:
		    bd.listaaPullot();
		    break;
		default:
		    break;
	    }
	} while (valinta != 0);

    }

    private static void valikko() {
	System.out.println("\n*** LIMSA-AUTOMAATTI ***");
	System.out.println("1) Lisää rahaa koneeseen");
	System.out.println("2) Osta pullo");
	System.out.println("3) Ota rahat ulos");
	System.out.println("4) Listaa koneessa olevat pullot");
	System.out.println("0) Lopeta");
	System.out.print("Valintasi: ");
    }

}
