package pullokone;

/**
 *
 * @author p4464
 */
public class Bottle {
    private String name;
    private String manufacturer;
    private double total_energy;
    private double koko;
    private double hinta;
    
    Bottle(){
        name = "Pepsi Max";
        manufacturer = "Pepsi";
        total_energy = 0.3;
        koko = 0.5;
        hinta = 1.80;
    }
    
    Bottle(String uName, String uManuf, double totE, double uKoko, double uHinta){
        name = uName;
        manufacturer = uManuf;
        total_energy = totE;
        koko = uKoko;
        hinta = uHinta;        
    }
    
    public String getName(){
        return name;
    }
        public String getManufacturer(){
        return manufacturer;
    }
        public double getEnergy(){
        return total_energy;
    }
    public double getHinta(){
        return hinta;
    }
    public double getKoko(){
        return koko;
    }
}

