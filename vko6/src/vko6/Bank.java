package vko6;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author p4464
 */
public class Bank {

    private final Map<String, Account> m;

    public Bank() {
	m = new HashMap<>();
    }

    public void luoTili(String numero, int rahaMäärä) {
	Account t;
	t = new Account.Tili(numero, rahaMäärä);
	m.put(numero, t);
	System.out.println("Tili luotu.");
    }

    public void luoTili(String numero, int rahaMäärä, int luottoMäärä) {
	Account t;
	t = new Account.LuottoTili(numero, rahaMäärä, luottoMäärä);
	m.put(numero, t);
	System.out.println("Tili luotu.");

	//System.out.printf("Pankkiin lisätään: %s,%d,%d\n", numero, rahaMäärä, luottoMäärä);
    }

    public void poistaTili(String numero) {
	m.remove(numero);
	System.out.println("Tili poistettu.");
    }

    public void nosta(String numero, int rahaMäärä) {
	Account t = m.get(numero);
	t.nosta(rahaMäärä);
	//System.out.printf("Nostetaan tililtä: %s rahaa %d\n", numero, rahaMäärä);
    }

    public void talleta(String numero, int rahaMäärä) {
	if (m.containsKey(numero)) {
	    Account t = m.get(numero);
	    t.talleta(rahaMäärä);
	}
	//System.out.printf("Talletetaan tilille: %s rahaa %d\n", numero, rahaMäärä);
    }

    private Account etsi(String numero) {
	return m.get(numero);
    }

    public void tulosta(String numero) {
	etsi(numero).esitä();
	System.out.println("");
    }

    public void kaikki() {
	for (Account i : m.values()) {
	    i.esitä();
	    System.out.println("");
	}
    }

}
