package pullokone;

import java.util.ArrayList;
import java.util.Locale;

/**
 *
 * @author p4464
 */
public class BottleDispenser {

    private int bottles;
    private double money;
    private ArrayList<Bottle> bottle_array;

    public BottleDispenser() {
	bottles = 6;
	money = 0;
	bottle_array = new ArrayList<>();

	bottle_array.add(new Bottle());
	bottle_array.add(new Bottle("Pepsi Max", "Pepsi", 0, 1.5, 2.2));
	bottle_array.add(new Bottle("Coca-Cola Zero", "Pepsi", 0, 0.5, 2.0));
	bottle_array.add(new Bottle("Coca-Cola Zero", "Pepsi", 0, 1.5, 2.5));
	bottle_array.add(new Bottle("Fanta Zero", "Pepsi", 0, 0.5, 1.95));
	bottle_array.add(new Bottle("Fanta Zero", "Pepsi", 0, 0.5, 1.95));
    }

    public void addMoney() {
	money += 1;
	System.out.println("Klink! Lisää rahaa laitteeseen!");
    }

    public void buyBottle(int idx) {
	if (bottles <= 0) {
	    System.out.println("Pullot ovat loppu");
	    return;
	}
	Bottle b = bottle_array.remove(idx - 1);
	if (money <= b.getHinta()) {
	    System.out.println("Syötä rahaa ensin!");
	    bottle_array.add(b);
	} else {
	    bottles -= 1;
	    money -= b.getHinta();
	    System.out.printf("KACHUNK! %s tipahti masiinasta!\n", b.getName());
	}
    }

    public void returnMoney() {
	System.out.printf("Klink klink. Sinne menivät rahat! Rahaa tuli ulos %.2f€\n",money);
	money = 0;
    }

    public void listaaPullot() {
	Bottle p;
	for (int i = 1; i <= bottle_array.size(); i++) {
	    p = bottle_array.get(i - 1);
	    System.out.printf("%d. Nimi: %s\n", i, p.getName());
	    System.out.printf(Locale.US, "\tKoko: %.1f\tHinta: %s\n", p.getKoko(), Double.toString(p.getHinta()));
	}
    }
}
