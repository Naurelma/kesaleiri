/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vko8;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 *
 * @author p4464
 */
public class FXMLDocumentController implements Initializable {

    private BottleDispenser bd;

    private Bottle last;

    @FXML
    private TextField amt;

    @FXML
    private TextArea alue;

    @FXML
    private Slider liukuri;
    @FXML
    private ComboBox mallit;
    @FXML
    private ComboBox koot;

    @FXML
    private void handleKolikko(ActionEvent event) {
	bd.addMoney(liukuri.getValue());
	liukuri.adjustValue(0);
	paivita();
    }

    @FXML
    private void handleOsta(ActionEvent event) {
	Object malli = mallit.getValue();
	Object koko = koot.getValue();
	// Kuitissa pitää olla tieto viimeisimmästä ostoksesta
	Bottle tmp = bd.buyBottle((String) malli, Double.parseDouble((String) koko));
	if (tmp != null) {
	    last = tmp;
	}
	paivita();
    }

    @FXML
    private void handlePalautus(ActionEvent event) {
	bd.returnMoney();
	paivita();
    }

    @FXML
    private void handleKuitti(ActionEvent event) {
	if (last != null) {
	    BufferedWriter bw = null;
	    try {
		bw = new BufferedWriter(new FileWriter("kuitti.txt"));
		bw.write("Kuitti Pullokoneesta:");
		bw.newLine();
		bw.write(last.toString());
		bw.newLine();
		bw.flush();
		bw.close();
	    } catch (IOException ex) {
		System.out.println("Io Virhe");
	    } finally {
		try {
		    if (bw != null) {
			bw.close();
		    }
		} catch (IOException ex) {
		    System.out.println("Io Virhe");
		}
	    }
	}
	paivita();
    }

    private void paivita() {
	amt.setText(String.format("%.2f", bd.getMoney()));
	alue.setText(bd.listaaPullot());

    }

    private void populateValikot() {
	BottleDispenser.Tiedot t = bd.pullojenTyypit();
	ObservableList items = koot.getItems();
	for (String d : t.getKoot()) {
	    items.add(d);
	    koot.setValue(d);
	}

	ObservableList items1 = mallit.getItems();
	for (String s : t.getNimet()) {
	    items1.add(s);
	    mallit.setValue(s);

	}
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
	bd = BottleDispenser.getInstance();
	paivita();
	populateValikot();
    }

}
