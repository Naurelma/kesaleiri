/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vko9;

import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author p4464
 */
public class NäytösLista {

    private ArrayList<String> lista;
    private Document doc;
    private static Map<Integer, Map<String, NäytösLista>> instanssit;
    //koska en halua hakea tietoja kahteen kertaan

    private NäytösLista(int id, String date) {
	String timeStamp;
	if (date == null) {
	    timeStamp = new SimpleDateFormat("dd.MM.yyyy").format(Calendar.getInstance().getTime());
	} else {

	    timeStamp = date;
	}
	//System.out.println(timeStamp);
	String url = String.format("https://www.finnkino.fi/xml/Schedule/?area=%d&dt=%s", id, timeStamp);
	//System.out.println(url);
	convert(PageLoader.gatherData(url));
	lista = new ArrayList<>();

    }

    public static NäytösLista getInstance(int id, String date) {
	if (instanssit == null) {
	    instanssit = new HashMap<>();
	}
	if (!instanssit.containsKey(id)) {
	    HashMap<String, NäytösLista> tmp = new HashMap<>();
	    tmp.put(date, new NäytösLista(id, date));
	    instanssit.put(id, tmp);
	} else if (!instanssit.get(id).containsKey(date)) {
	    instanssit.get(id).put(date, new NäytösLista(id, date));
	}

	return instanssit.get(id).get(date);
    }

    private void convert(String total) {
	try {
	    DocumentBuilderFactory dbFactory
		    = DocumentBuilderFactory.newInstance();
	    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();

	    doc = dBuilder.parse(new InputSource(new StringReader(total)));
	    doc.getDocumentElement().normalize();

	} catch (ParserConfigurationException e) {
	    System.err.println("Caught ParserConfigurationException!");
	} catch (IOException e) {
	    System.err.println("Caught IOException!");
	} catch (SAXException e) {
	    System.err.println("Caught SAXException!");
	}
    }

    public ArrayList<String> getNimet() {
	if (lista == null) {
	    paivita(-1, -1, -1, -1, "");
	}
	return lista;
    }
//12:30:00

    public void paivita(int aT, int aM, int lT, int lM, String toivottuNimi) {
	NodeList arvot = doc.getElementsByTagName("Show");
	lista.clear();
	if (lT < 0) {
	    lT = 999;
	}
	if (lM < 0) {
	    lM = 999;
	}
	for (int i = 0; i < arvot.getLength(); i++) {

	    Element item = (Element) arvot.item(i);

	    String alku = item.getElementsByTagName("dttmShowStart").item(0).getTextContent();
	    String[] ajatA = alku.split("T")[1].split(":");
	    Integer alkuT = Integer.parseInt(ajatA[0]);
	    Integer alkuM = Integer.parseInt(ajatA[1]);

	    if (alkuT < aT) {
		continue;
	    } else if (alkuT == aT && alkuM < aM) {
		continue;
	    }
	    //Myöhäisin aloitus aika
	    if (alkuT > lT) {
		continue;
	    } else if (alkuT == lT && alkuM > lM) {
		continue;
	    }
	    /*
	    String loppu = item.getElementsByTagName("dttmShowEnd").item(0).getTextContent();
	    String[] ajatL = loppu.split("T")[1].split(":");
	    int loppuT = Integer.parseInt(ajatL[0]);
	    int loppuM = Integer.parseInt(ajatL[1]);
	    
	    if (loppuT > lT) {
	    continue;
	    } else if (loppuT == lT && loppuM > lM) {
	    continue;
	    }
	     */
	    //System.out.println(alku);
	    String nimi = String.format("%-60s", item.getElementsByTagName("Title").item(0).getTextContent());
	    nimi = nimi.substring(0, 60);
	    
	    String aika = String.format("%02d",alkuT)+ ":" + String.format("%02d",alkuM);

	    if (!toivottuNimi.isEmpty()) {
		if (!nimi.toLowerCase().contains(toivottuNimi.toLowerCase())) {
		    continue;

		}
	    }
	    lista.add("  "+nimi + " " + aika);

	}

    }
}
