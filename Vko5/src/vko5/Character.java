package vko5;

/**
 *
 * @author p4464
 */
public class Character {

    protected Ase ase;

    public Character(int numero) {
	//
	switch (numero) {
	    case 1:
	
		ase = new Veitsi();
		break;
	    case 2:
		
		ase = new Kirves();
		break;
	    case 3:
		
		ase = new Miekka();
		break;
	    case 4:
		
		ase = new Nuija();
		break;
	    default:
		ase = new Ase();

	}
	
    }

    public Character luo(int tyyppi, int num) {
	Character r;
	switch (tyyppi) {
	    case 1:

		r = new King(num);
		break;
	    case 2:
		r = new Ritari(num);
		break;
	    case 3:
		r = new Kuningatar(num);
		break;
	    case 4:
		r = new Peikko(num);
		break;
	    default:
		r = new Character(num);
		break;
	}
	return r;
    }

    public String fight() {
	return "tyhjä tappelee aseella " + ase.fight();
    }

    public class Ase {

	public String fight() {
	    return "tyhjä";
	}
    }

    public class Miekka extends Ase {

	@Override
	public String fight() {
	    return "Sword";
	}
    }

    public class Veitsi extends Ase {

	@Override
	public String fight() {
	    return "Knife";
	}
    }

    public class Kirves extends Ase {

	@Override
	public String fight() {
	    return "Kirves";
	}
    }

    public class Nuija extends Ase {

	@Override
	public String fight() {
	    return "Nuija";
	}
    }

    public class King extends Character {

	public King(int numero) {
	    super(numero);
	}

	@Override
	public String fight() {
	    return "King tappelee aseella " + ase.fight();
	}
    }

    public class Ritari extends Character {

	public Ritari(int numero) {
	    super(numero);
	}

	@Override
	public String fight() {
	    return "Knight tappelee aseella " + ase.fight();
	}

    }

    public class Kuningatar extends Character {

	public Kuningatar(int numero) {
	    super(numero);
	}

	@Override
	public String fight() {
	    return "Queen tappelee aseella " + ase.fight();
	}

    }

    public class Peikko extends Character {

	public Peikko(int numero) {
	    super(numero);
	}

	@Override
	public String fight() {
	    return "Troll tappelee aseella " + ase.fight();
	}

    }
}
