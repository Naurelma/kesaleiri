/*package vko6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;


public class Mainclass {
    
    
    public static void main(String[] args) throws IOException {
	Map<String, Tili> m = new HashMap<>();
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	int valinta, raha, raja;
	Tili t;
	String numero;
	do {
	    System.out.println("\n*** PANKKIJÄRJESTELMÄ ***");
	    System.out.println("1) Lisää tavallinen tili");
	    System.out.println("2) Lisää luotollinen tili");
	    System.out.println("3) Tallenna tilille rahaa");
	    System.out.println("4) Nosta tililtä");
	    System.out.println("5) Poista tili");
	    System.out.println("6) Tulosta tili");
	    System.out.println("7) Tulosta kaikki tilit");
	    System.out.println("0) Lopeta");
	    System.out.print("Valintasi: ");
	    
	    valinta = Integer.parseInt(br.readLine());
	    switch (valinta) {
		case 0:
		    break;
		case 1:
		    System.out.print("Syötä tilinumero: ");
		    numero = br.readLine();
		    System.out.print("Syötä rahamäärä: ");
		    raha = Integer.parseInt(br.readLine());
		    t = new Tili(numero, raha);
		    t.esitä();
		    m.put(numero, t);
		    break;
		case 2:
		    System.out.print("Syötä tilinumero: ");
		    numero = br.readLine();
		    System.out.print("Syötä rahamäärä: ");
		    raha = Integer.parseInt(br.readLine());
		    System.out.print("Syötä luottoraja: ");
		    raja = Integer.parseInt(br.readLine());
		    t = new LuottoTili(numero, raha, raja);
		    //System.out.println(t instanceof LuottoTili);
		    t.esitä();
		    m.put(numero, t);
		    break;
		case 3:
		    System.out.print("Syötä tilinumero: ");
		    numero = br.readLine();
		    if (m.containsKey(numero)) {
			System.out.print("Syötä rahamäärä: ");
			raha = Integer.parseInt(br.readLine());
			t = m.get(numero);
			t.talleta(raha);
			t.esitä();
		    } else {
			System.out.println("Tilinumeroa ei löydy");
		    }
		    break;
		case 4:
		    System.out.print("Syötä tilinumero: ");
		    numero = br.readLine();
		    if (m.containsKey(numero)) {
			System.out.print("Syötä rahamäärä: ");
			raha = Integer.parseInt(br.readLine());
			t = m.get(numero);
			t.nosta(raha);
		    } else {
			System.out.println("Tilinumeroa ei löydy");
		    }
		    break;
		case 5:
		    System.out.print("Syötä tilinumero: ");
		    numero = br.readLine();
		    m.remove(numero);
		    System.out.printf("Tilinumero: %s\n", numero);
		    break;
		case 6:
		    System.out.print("Syötä tilinumero: ");
		    numero = br.readLine();
		    if (m.containsKey(numero)) {
			t = m.get(numero);
			t.esitä();
		    } else {
			System.out.println("Tilinumeroa ei löydy");
		    }
		    break;
		case 7:
		    for (Map.Entry<String, Tili> entry : m.entrySet()) {
			entry.getValue().esitä();
			System.out.println("");
		    }
		    break;
		default:
		    System.out.println("Valinta ei kelpaa.");
		    break;
		    
	    }
	} while (valinta != 0);
    }
    
    public static class Tili {
	
	protected String tilinumero;
	protected int rahaMäärä;
	
	public Tili(String numero, int määrä) {
	    tilinumero = numero;
	    rahaMäärä = määrä;
	}
	
	public void esitä() {
	    System.out.printf("Tilinumero: %s\n", tilinumero);
	    System.out.printf("Rahamäärä: %d\n", rahaMäärä);
	}
	
	public void nosta(int määrä) {
	    if (rahaMäärä - määrä < 0) {
		System.out.println("Rahat eivät riitä");
	    } else {
		rahaMäärä -= määrä;
	    }
	}
	
	public void talleta(int määrä) {
	    rahaMäärä += määrä;
	}
	
    }
    
    
    public static class LuottoTili extends Tili {
	
	private final int luottoRaja;
	
	public LuottoTili(String numero, int määrä, int raja) {
	    super(numero, määrä);
	    luottoRaja = raja;
	}
	
	@Override
	public void nosta(int määrä) {
	    if (luottoRaja + rahaMäärä - määrä < 0) {
		System.out.println("Luottoraja ylitetty!");
	    } else {
		rahaMäärä -= määrä;
	    }
	}
	
	@Override
	public void esitä() {
	    super.esitä();
	    System.out.printf("Luotto: %d\n", luottoRaja);
	    
	}
    }
}*/