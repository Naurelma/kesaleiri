package vko7;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;

/**
 *
 * @author p4464
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private Label label;

    @FXML
    private TextField kenttä;

    @FXML
    private void handleButtonAction(ActionEvent event) {
	System.out.println("Hello World");

	label.setText(kenttä.getText());
	kenttä.setText("Hello World");
	kenttä.requestFocus();
    }

    /*@FXML
    private void tekstikentänAction(ActionEvent e) {
    label.setText(kenttä.getText());
    //kenttä.setText("");
    
    }*/
    @FXML
    private void teksti(KeyEvent e) {
	if (e.getCharacter().trim().isEmpty()) {
	    if (!e.getCharacter().equals(" ")) {
		label.setText(kenttä.getText());
		kenttä.setText("");
		return;
	    }
	}
	label.setText(kenttä.getText() + e.getCharacter());
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
	// TODO
    }

}
