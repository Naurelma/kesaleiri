/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vko9;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author p4464
 */
public class Vko9 extends Application {

    @Override
    public void start(Stage stage) throws Exception {
	Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));

	Scene scene = new Scene(root);
	stage.setTitle("Elokuvien listaaja");
	stage.setScene(scene);
	stage.show();
    }

    /**
     * @param args the command line arguments
     * @throws java.lang.Exception
     */
    public static void main(String[] args) throws Exception {

	launch(args);
    }

    /* 
    hakutoiminto, pelkästään valitusta teatterista;
    kaikkien teatterien tapauksessa lisää listaan Boldattuna teatterin nimi
    */
}
