package vko8;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

/**
 *
 * @author p4464
 */
public class BottleDispenser {

    private static BottleDispenser bd = null;

    private int bottles;
    private double money;
    private ArrayList<Bottle> bottle_array;

    private BottleDispenser() {
	bottles = 6;
	money = 0;
	bottle_array = new ArrayList<>();

	bottle_array.add(new Bottle());
	bottle_array.add(new Bottle("Pepsi Max", "Pepsi", 0, 1.5, 2.2));
	bottle_array.add(new Bottle("Coca-Cola Zero", "Pepsi", 0, 0.5, 2.0));
	bottle_array.add(new Bottle("Coca-Cola Zero", "Pepsi", 0, 1.5, 2.5));
	bottle_array.add(new Bottle("Fanta Zero", "Pepsi", 0, 0.5, 1.95));
	bottle_array.add(new Bottle("Fanta Zero", "Pepsi", 0, 0.5, 1.95));
    }

    public static BottleDispenser getInstance() {
	if (bd == null) {
	    bd = new BottleDispenser();
	}
	return bd;
    }

    public double getMoney() {
	return money;
    }

    public void addMoney(double amt) {
	money += amt;
	System.out.println("Klink! Lisää rahaa laitteeseen!");
    }

    public Bottle buyBottle(int idx) {
	if (bottles <= 0) {
	    System.out.println("Pullot ovat loppu");
	    return null;
	}
	Bottle b = bottle_array.remove(idx - 1);
	if (money <= b.getHinta()) {
	    System.out.println("Syötä rahaa ensin!");
	    bottle_array.add(idx - 1, b);
	    return null;
	} else {
	    bottles -= 1;
	    money -= b.getHinta();
	    System.out.printf("KACHUNK! %s tipahti masiinasta!\n", b.getName());
	    return b;
	}
    }

    public void returnMoney() {
	System.out.printf("Klink klink. Sinne menivät rahat! Rahaa tuli ulos %.2f€\n", money);
	money = 0;
    }
    //lisätty toiminnallisuus
    public Bottle buyBottle(String nimi, double koko) {
	int i = 1;
	for (Bottle b : bottle_array) {
	    if (b.getName().equals(nimi) && b.getKoko() == koko) {
		return buyBottle(i);
	    }
	    i++;
	}
	System.out.println("Toivottua pulloa ei löytynyt.");
	return null;
    }


    public Tiedot pullojenTyypit() {
	Tiedot t = new Tiedot();
	for (Bottle b : bottle_array) {
	    t.add(b.getName());
	    t.add(b.getKoko());
	    
	}
	return t;
    }

    public class Tiedot {

	private Set<String> nimet;
	private Set<String> koot;

	public Tiedot() {
	    nimet = new HashSet<>();
	    koot = new HashSet<>();
	}

	public void add(String nimi) {
	    nimet.add(nimi);
	}

	public void add(double koko) {
	    koot.add(Double.toString(koko));
	}

	public Set<String> getNimet() {
	    return nimet;
	}

	public Set<String> getKoot() {
	    return koot;
	}
    }

    public String listaaPullot() {
	Bottle p;
	String s = "";
	for (int i = 1; i <= bottle_array.size(); i++) {
	    p = bottle_array.get(i - 1);

	    s += String.format("%d. Nimi: %s\n", i, p.getName());
	    s += String.format(Locale.US, "\tKoko: %.1f\tHinta: %s\n", p.getKoko(), Double.toString(p.getHinta()));
	}
	return s;
    }
}
