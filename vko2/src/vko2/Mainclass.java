package vko2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author p4464
 */
public class Mainclass {

    /**
     * @param args the command line arguments
     * 
     */
    public static void main(String[] args) {
        try {
            BufferedReader in;
            in = new BufferedReader(new InputStreamReader(System.in));
            
            System.out.print("Anna koiralle nimi: ");
            String txt = in.readLine();
            Dog rekku = new Dog(txt);
            
             do {
                System.out.print("Mitä koira sanoo: ");
                txt = in.readLine();
            } while ( ! rekku.speak(txt));
   
        } catch (IOException ex) {
            System.out.println(ex);
        }
        

    }
    
}

