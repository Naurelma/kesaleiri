package vko2;

import java.util.Scanner;

/**
 *
 * @author p4464
 */
public class Dog {
    
    private String nimi;
    private String lausahdus;
    
    Dog(String uNimi){
        nimi = uNimi;
        nimi = nimi.trim();
        if (nimi.isEmpty()){
            nimi = "Doge";
        }
        System.out.println("Hei, nimeni on "+ nimi);
    }
    
    public boolean speak(String uLausahdus){
        lausahdus = uLausahdus;
        lausahdus = lausahdus.trim();
        boolean arvo = true;
        if (lausahdus.isEmpty()){
            lausahdus = "Much wow!";
            arvo = false;
        }
        Scanner s = new Scanner(lausahdus);
        while(s.hasNext()){
            if (s.hasNextBoolean()){
               System.out.print("Such boolean: ");
            } else if (s.hasNextInt()){
                System.out.print("Such integer: ");
            } 
            System.out.println(s.next());
        }
        //System.out.println(nimi+": "+lausahdus);
        return arvo;
    }
    
}
