/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vko9;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author p4464
 */
public class PageLoader {

    public static String gatherData(String lahde) {
	try {
	    URL url = new URL(lahde);
	    BufferedReader in = new BufferedReader(
		    new InputStreamReader(url.openStream(), "UTF-8"));
	    String inputLine, total = "";
	    while ((inputLine = in.readLine()) != null) {
		total += inputLine + "\n";
	    }
	    in.close();
	    //System.out.println(total);
	    return total;
	} catch (MalformedURLException ex) {
	    Logger.getLogger(PageLoader.class.getName()).log(Level.SEVERE, null, ex);
	} catch (IOException ex) {
	    System.out.println("");
	}
	return null;
    }
}
